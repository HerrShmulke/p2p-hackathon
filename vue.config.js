module.exports = {
  transpileDependencies: ['vuetify'],
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
          @use "sass:map";
          @import "~@/assets/scss/vars.scss";
        `,
      },
    },
  },
};
