import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import { initializePeer } from "./services/peerjs";

import "@/assets/scss/base.scss";
import "@mdi/font/css/materialdesignicons.css";

Vue.config.productionTip = false;

initializePeer().then(() => {
  new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App),
  }).$mount("#app");
});
