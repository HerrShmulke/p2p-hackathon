import {REGISTRATION} from "@/constants/routerName";


export default function checkUserData({ next }) {
    let dataUser = localStorage.getItem('dataUser');
    if (dataUser) return next();
    return next({ name: REGISTRATION });
}