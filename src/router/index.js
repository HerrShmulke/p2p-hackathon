import Vue from "vue";
import VueRouter from "vue-router";
import * as layoutsNames from "@/constants/layouts";
import * as routerNames from "@/constants/routerName";
import checkUserData from "@/router/middleware/checkUserData";
import createMiddlewarePipeline from "@/router/createMiddlewarePipeline";
import collectMatchedMiddleware from "@/utils/router";

Vue.use(VueRouter);

/** @type {import('vue-router').RouteConfig[]} */
const routes = [
  {
    path: "/login",
    name: routerNames.LOGIN,
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
    meta: {
      layout: layoutsNames.LOGIN,
    },
  },
  {
    path: "/registration",
    name: routerNames.REGISTRATION,
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Registration"),
    meta: {
      layout: layoutsNames.LOGIN,
    },
  },
  {
    path: "/profile",
    name: routerNames.PROFILE,
    component: () => import("../views/Profile"),
    meta: {
      layout: layoutsNames.DEFAULT,
      middleware: [checkUserData],
    },
  },
  {
    path: "/",
    name: routerNames.LISTS,
    component: () => import(/* webpackChunkName: "lists" */ "../views/Lists"),
    meta: {
      layout: layoutsNames.DEFAULT,
      middleware: [checkUserData],
      backRoute: null,
    },
  },
  {
    path: "/shopping",
    name: routerNames.SHOPPING,
    component: () =>
      import(/* webpackChunkName: "shopping" */ "../views/Shopping.vue"),
    meta: {
      layout: layoutsNames.DEFAULT,
      middleware: [checkUserData],
      backRoute: { name: routerNames.LISTS },
    },
    props: (route) => {
      return {
        userId: route.query.userId,
        listId: route.query.id,
      };
    },
  },
  {
    path: "/item",
    name: routerNames.ITEM,
    component: () => import("../views/Item.vue"),
    meta: {
      layout: layoutsNames.DEFAULT,
      middleware: [checkUserData],
      backRoute: routerNames.SHOPPING,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const middleware = collectMatchedMiddleware(to.matched);

  const firstMiddleware = middleware[0];
  if (!firstMiddleware) {
    next();
    return;
  }

  const context = { to, from, next };
  createMiddlewarePipeline(context, middleware)();
});

export default router;
