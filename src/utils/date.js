import { ru } from 'date-fns/locale';
import { format as _format } from 'date-fns';

export function format(date, stringFormat, options) {
  return _format(date, stringFormat, {
    locale: ru,
    ...options,
  });
}
