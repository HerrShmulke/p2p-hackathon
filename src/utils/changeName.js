function changeName(newName) {
  const data = localStorage.getItem("dataUser");
  if (!data) return;
  const newData = JSON.stringify({
    ...JSON.parse(data),
    name: newName,
  });
  localStorage.setItem("dataUser", newData);
}

export default changeName;
