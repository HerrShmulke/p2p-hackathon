import getName from "./getName";
import changeName from "./changeName";

export { getName, changeName };
