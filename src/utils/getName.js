function getName() {
  const data = localStorage.getItem("dataUser");
  if (!data) return "Я";
  return JSON.parse(data).name;
}

export default getName;
