import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

console.log('createPersistedState', createPersistedState)

Vue.use(Vuex)



export default new Vuex.Store({
  state: {
    allUsers: [],
    myId: '',
  },
  mutations: {
    addMyList(state, list) {
      const myLists = state.allUsers.find(user => user.userId === state.myId).lists;
      const userIndex = state.allUsers.findIndex(user => user.userId === state.myId);
      myLists.push(list);
      state.allUsers[userIndex].list = myLists;
    },
    addNewUser(state, user) {
      state.allUsers.push(user)
    },
    createMe(state, name) {
      // console.log(localStorage.getItem(\'dataUser\'))
      const {peerId} = JSON.parse(localStorage.getItem('dataUser'))
      state.myId = peerId
      state.allUsers.push(
        {
          userId: peerId,
          name: name,
          lists: [],
        }
      )
    }

  },
  getters: {
    getListById: state => id => {
      return state.myLists.find(list => list.id === id);
    },
    getOnlyMyList(state) {
      return state.allUsers.find(user => user.userId === state.myId).lists
    },
    getAllList(state) {
      const allList = [];
      state.allUsers.forEach(user => {
        allList.push(...user.lists)
      })
      return allList;
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [createPersistedState()],
})
