import Peer from "peerjs";
import store from "@/store";

let peer = null;
let dataConnections = [];

function processData(data) {
  console.log(data);
  switch (data.type) {
    case "sync": {
      const myLists = store.getters.getOnlyMyLists;
      store.commit("addNewUser", {
        userId: data.userId,
        name: "john",
        lists: [],
      });
      console.log(myLists);
      sendPeerMessage({
        type: "getSyncData",
        id: store.state.myId,
        lists: myLists,
      });
    }
    case "getSyncData": {
      store.commit("addListById", { list: data.lists[0], id });
    }
  }
}

export const initializePeer = () =>
  new Promise((resolve) => {
    if (peer) resolve(true);
    const savedUser = localStorage.getItem("dataUser") || undefined;
    const savedId = savedUser ? JSON.parse(savedUser).peerId : undefined;
    peer = new Peer(savedId);
    if (savedUser) resolve();
    peer.on("connection", (res) => {
      console.log("connection");
      res.on("data", processData);
      dataConnections.push(res);
    });
    peer.on("error", (error) => console.log(error));
    peer.on("open", (peerId) => {
      if (!savedUser) {
        const dataUser = JSON.stringify({
          peerId,
        });
        localStorage.setItem("dataUser", dataUser);
        resolve(true);
      }
    });
  });

export const connectToDestPeer = (destId) =>
  new Promise((resolve) => {
    console.log(destId);
    if (!peer) return;

    const newDataConnection = peer.connect(destId);
    console.log("newDataConnection", newDataConnection);
    newDataConnection.on("open", () => resolve());
    newDataConnection.on("error", (error) => console.log(error));
    newDataConnection.on("data", processData);
    newDataConnection.on("open", () => {
      dataConnections.push(newDataConnection);
      resolve();
    });
  });

export function sendPeerMessage(message) {
  if (!dataConnections.length) return;
  dataConnections.forEach((connection) => {
    connection.send(message);
  });
}
